# Market Wave Theme

## Contents of this file

- Introduction
- Requirements
- Installation
- Live Demo Steps
- Configuration
- Maintainers

## Introduction

Market Wave is a user-friendly e-commerce theme that works well with
Bootstrap and Drupal Commerce. It offers a stylish design and powerful
features to create a smooth shopping experience. You can easily customize
your store and manage products, orders, and payments with ease.

Key Features:
- Fully responsive design, optimized for mobile and desktop
- Bootstrap-based for a flexible and modern layout
- Easy integration with Drupal Commerce
- Pre-built product page templates
- Customizable product grids and shopping cart
- Seamless checkout process with multiple payment gateways
- Fully configurable headers and footers
- Clean and intuitive user interface for both store owners and customers
- Advanced customization options for developers

For a full description of the theme, visit the [project page](https://www.drupal.org/project/market_wave).  
Use the [Issue queue](https://www.drupal.org/project/issues/market_wave) to submit bug reports,
feature suggestions, or track changes.

## Requirements

This theme requires:
- **Drupal core >= 9.0**
- **Drupal Commerce** for e-commerce functionality.

**Note**: While Market Wave works out of the box, it is recommended to have
a basic understanding of Bootstrap and Drupal Commerce
for advanced customizations.

## Installation

To install Market Wave:

1. Install the theme as you would any contributed Drupal theme.
2. Visit the [project page](https://www.drupal.org/project/market_wave) or the Drupal documentation for further installation details.

### Set Market Wave as the default theme

1. Navigate to Admin > Appearance.
2. Install the Market Wave theme by clicking "Install" under Market Wave.
3. At the bottom of the page, set Market Wave as your default theme.

## Live Demo Steps

To preview the theme in action, follow these steps:

1. Click the Live Demo button.
2. Type "market_wave" in the project field.
3. Select a Drupal version.
4. Click Launch Sandbox.
5. Wait a few minutes for the sandbox to load.

## Configuration

Market Wave comes with several configuration options to help you set
up your store:

### Store Settings
- Navigate to Admin > Appearance > Settings > Market Wave to configure
the store layout.
- Customize product grids, categories, and the checkout process.

### Header and Footer Configuration
- Customize the header and footer layout, background colors, and content
directly from the theme settings.

### Payment and Checkout
- Integrate with Drupal Commerce payment gateways to configure your store's
payment options.

For more advanced customization, you can easily create a subtheme and override
the provided settings and styles.

## Maintainers

Current maintainers:
- Yogesh Kumar ([@yogeshk](https://www.drupal.org/u/yogeshk))
- Abhiyanshu Rawat ([@abhiyanshu](https://www.drupal.org/u/abhiyanshu))

If you would like to contribute, please visit the [Issue queue](https://www.drupal.org/project/issues/market_wave) to report issues or submit patches.
